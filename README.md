# [INSERT TEAM NAME HERE]

This is a detailed description of your project.


## Project Requirements

Once your project has been completed please fill out the links below. These are a part of the rubric and must be completed before the submission deadline

**[Scrum Board URL](https://trello.com/)** | 
**[Deployment URL](https://heroku.com/)** | 
**[Kapstone Pitch URL](https://google.com/)** | 
**[Group Retrospective Document URL](https://google.com/)** |
**[Kapstone Feedback Form](https://docs.google.com/forms/d/1yeIyQH6ZE6y5Z0qB2i8yW5_1Gzfxs8YiJsNlcyjR0WA/edit)**

> **Note:**  **Points will be deducted** if this is not filled in correctly. Also each one of these **MUST** live in their own document. For example DO **NOT** put your retrospective in your scrum board as it will not be counted and you will lose points.

## About Us

Tell us about your team, who was involved, what role did they fulfill and what did they work on (the more details the better). Also please choose ONE of the staff below that was your investor and remove the others.

|      Name          |Scrum Role                          |I worked on                         |
|----------------|-------------------------------|-----------------------------|
|Vince/Tj/Jacob/Jake/Kano/Suri|`Product Investor`            |Investing            |
|No-Kap Kapstone          |`QA`            |Fill me out...            |
|Po Po          |`Product Owner`| Fill me out... |
|Scrum Lord          |`Scrum Master`| Fill me out... |
|Git Commit          |`Dev`| Fill me out... |
|Git Merge          |`Dev`| Fill me out... |
|Rimraf Node_modules          |`Dev`| Fill me out... |

